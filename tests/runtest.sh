#!/bin/bash

# get tpm simulator code
IBMTPM_VERSION=1661
wget --no-check-certificate https://downloads.sourceforge.net/project/ibmswtpm2/ibmtpm$IBMTPM_VERSION.tar.gz

res="$?"

if [[ "$res" -ne 0 ]]; then
    echo "wget failed"
    exit 1
fi

# unpackage and build the source
mkdir ibmtpm
pushd ibmtpm
tar xf ../ibmtpm$IBMTPM_VERSION.tar.gz
pushd src
# fixup for openssl 3
sed -i -e "s|OPENSSL_VERSION_NUMBER >= 0x10200000L|OPENSSL_VERSION_NUMBER > 0x30000000L|" TpmToOsslMath.h
sed -i -e "s|CCFLAGS = -Wall|CCFLAGS = -Wall -Wno-error=deprecated-declarations|" makefile
make

res="$?"

if [[ "$res" -ne 0 ]]; then
    echo "make of ibmtpm failed"
    popd
    popd
    rm -rf ibmtpm ibmtpm$IBMTPM_VERSION.tar.gz
    exit 1
fi
popd
popd

# get tpm2-tools tests
TPM2_TOOLS_VERSION=5.0
git clone https://github.com/01org/tpm2-tools.git
pushd tpm2-tools
git checkout -b test $TPM2_TOOLS_VERSION
pushd test/integration
sed -i -e 's/python/python3/g' helpers.sh
pushd tests
sed -i -e 's/python/python3/g' *.sh
# some tests aren't executable currently. Needs to be fixed upstream.
chmod +x *.sh
popd
popd
popd

TPM2_ABRMD=tpm2-abrmd
TPM2_SIM=tpm_server
TPM2_TOOLS_TEST_FIXTURES=`pwd`/tpm2-tools/test/integration/fixtures
PATH=`pwd`/ibmtpm/src/:.:$PATH
export TPM2_ABRMD TPM2_SIM TPM2_TOOLS_TEST_FIXTURES PATH

pushd tpm2-tools/test/integration
for t in `ls tests/*.sh`
do
    f=`basename $t`
    test=${f%%.*}
    /usr/share/automake-1.16/test-driver --test-name $test --log-file $test.log --trs-file $test.trs $t
done
all=`grep ":test-result:" *.trs | wc -l`;
pass=`grep ":test-result: PASS"  *.trs | wc -l`;
fail=`grep ":test-result: FAIL"  *.trs | wc -l`;
skip=`grep ":test-result: SKIP"  *.trs | wc -l`;
xfail=`grep ":test-result: XFAIL" *.trs | wc -l`;
xpass=`grep ":test-result: XPASS" *.trs | wc -l`;
error=`grep ":test-result: ERROR" *.trs | wc -l`;
if [[ $(($fail + $xpass + $error)) -eq 0 ]]; then
    success=0
else
    success=1
fi;
popd

echo PASSED: $pass
echo FAILED: $fail
echo SKIPPED: $skip
echo XFAIL: $xfail
echo XPASS: $xpass
echo ERROR: $error

exit $success
